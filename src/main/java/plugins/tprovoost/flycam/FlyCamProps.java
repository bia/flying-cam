package plugins.tprovoost.flycam;

import icy.gui.component.button.IcyButton;
import icy.gui.frame.IcyFrame;
import icy.main.Icy;
import icy.painter.Overlay;
import icy.preferences.PluginsPreferences;
import icy.preferences.XMLPreferences;
import icy.sequence.Sequence;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class FlyCamProps extends IcyFrame implements ActionListener
{
    private XMLPreferences prefs = PluginsPreferences.root(FlyCam.class);

    private JSlider sliderSpeed;
    private JCheckBox cboxInvertX;
    private JCheckBox cboxInvertY;

    private static final String PREF_X_INVERTED = "xinverted";
    private static final String PREF_Y_INVERTED = "yinverted";
    private static final String PREF_SPEED = "speed";
    private static final String PREF_KEY_FORWARD = "forward";
    private static final String PREF_KEY_BACKWARD = "backward";
    private static final String PREF_KEY_STRAFE_LEFT = "strafeLeft";
    private static final String PREF_KEY_STRAFE_RIGHT = "strafeRight";
    private static final String PREF_KEY_STRAFE_UP = "strafeUp";
    private static final String PREF_KEY_STRAFE_DOWN = "strafeDown";

    private static FlyCamProps singleton;
    private JPanel panelOptions;

    private IcyButton btnForward;
    private IcyButton btnBackward;
    private IcyButton btnStrafeLeft;
    private IcyButton btnStrafeRight;
    private IcyButton btnStrafeUp;
    private IcyButton btnStrafeDown;

    public int keyForward;
    public int keyBackward;
    public int keyStrafeLeft;
    public int keyStrafeRight;
    public int keyStrafeUp;
    public int keyStrafeDown;
    private JTextPane infoTextPane;

    /**
     * Singleton.
     */
    private FlyCamProps()
    {
        super("FlyCam Properties", true, true, false, true);

        initialize();

        infoTextPane.setText("Click on the image to capture or release mouse focus.\n"
                + "When mouse focus is captured the mouse cursor is hidden and the mouse / keys control the camera.");

        loadPrefs();
        setSize(new Dimension(260, 440));
        center();

        cboxInvertX.addActionListener(this);
        cboxInvertY.addActionListener(this);
        btnForward.addActionListener(this);
        btnBackward.addActionListener(this);
        btnStrafeLeft.addActionListener(this);
        btnStrafeRight.addActionListener(this);
        btnStrafeUp.addActionListener(this);
        btnStrafeDown.addActionListener(this);
    }

    private void initialize()
    {
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        panelOptions = new JPanel();
        panelOptions.setBorder(new TitledBorder(null, "Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        mainPanel.add(panelOptions);
        panelOptions.setLayout(new GridLayout(4, 1, 0, 0));

        cboxInvertX = new JCheckBox("Invert X-Axis");
        panelOptions.add(cboxInvertX);
        cboxInvertY = new JCheckBox("Invert Y-Axis");
        panelOptions.add(cboxInvertY);

        JLabel lblSpeed = new JLabel("Speed: ");
        panelOptions.add(lblSpeed);
        sliderSpeed = new JSlider(1, 100, 2);
        panelOptions.add(sliderSpeed);

        setContentPane(mainPanel);

        JPanel panelKeys = new JPanel();
        panelKeys.setBorder(new TitledBorder(null, "Keys", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        mainPanel.add(panelKeys);
        panelKeys.setLayout(new GridLayout(6, 0, 0, 0));

        JPanel panelForward = new JPanel();
        panelKeys.add(panelForward);
        panelForward.setLayout(new GridBagLayout());

        JLabel lblForward = new JLabel("Forward");
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 0.7;
        c.gridx = 0;
        c.gridy = 0;
        panelForward.add(lblForward, c);

        btnForward = new IcyButton("W");
        btnForward.setFlat(true);
        btnForward.setHorizontalAlignment(SwingConstants.CENTER);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0.3;
        c.gridx = 1;
        c.gridy = 0;
        panelForward.add(btnForward, c);

        JPanel panelBackward = new JPanel();
        panelKeys.add(panelBackward);
        panelBackward.setLayout(new GridBagLayout());

        JLabel lblBackward = new JLabel("Backward");
        c = new GridBagConstraints();
        c.weightx = 0.7;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 0;
        panelBackward.add(lblBackward, c);

        btnBackward = new IcyButton("S");
        btnBackward.setFlat(true);
        btnBackward.setHorizontalAlignment(SwingConstants.CENTER);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0.3;
        c.gridx = 1;
        c.gridy = 0;
        panelBackward.add(btnBackward, c);

        JPanel panelStrafeLeft = new JPanel();
        panelKeys.add(panelStrafeLeft);
        panelStrafeLeft.setLayout(new GridBagLayout());

        JLabel lblStrafeLeft = new JLabel("Strafe Left");
        c = new GridBagConstraints();
        c.weightx = 0.7;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 0;
        panelStrafeLeft.add(lblStrafeLeft, c);

        btnStrafeLeft = new IcyButton("A");
        btnStrafeLeft.setFlat(true);
        btnStrafeLeft.setHorizontalAlignment(SwingConstants.CENTER);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0.3;
        c.gridx = 1;
        c.gridy = 0;
        panelStrafeLeft.add(btnStrafeLeft, c);

        JPanel panelStrafeRight = new JPanel();
        panelKeys.add(panelStrafeRight);
        panelStrafeRight.setLayout(new GridBagLayout());

        JLabel lblStrafeRight = new JLabel("Strafe Right");
        c = new GridBagConstraints();
        c.weightx = 0.7;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 0;
        panelStrafeRight.add(lblStrafeRight, c);

        btnStrafeRight = new IcyButton("D");
        btnStrafeRight.setFlat(true);
        btnStrafeRight.setHorizontalAlignment(SwingConstants.CENTER);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0.3;
        c.gridx = 1;
        c.gridy = 0;
        panelStrafeRight.add(btnStrafeRight, c);

        JPanel panelStrafeUp = new JPanel();
        panelKeys.add(panelStrafeUp);
        panelStrafeUp.setLayout(new GridBagLayout());

        JLabel lblStrafeUp = new JLabel("Strafe Up");
        c = new GridBagConstraints();
        c.weightx = 0.7;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 0;
        panelStrafeUp.add(lblStrafeUp, c);

        btnStrafeUp = new IcyButton("C");
        btnStrafeUp.setFlat(true);
        btnStrafeUp.setHorizontalAlignment(SwingConstants.CENTER);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0.3;
        c.gridx = 1;
        c.gridy = 0;
        panelStrafeUp.add(btnStrafeUp, c);

        JPanel panelStrafeDown = new JPanel();
        panelKeys.add(panelStrafeDown);
        panelStrafeDown.setLayout(new GridBagLayout());

        JLabel lblStrafeDown = new JLabel("Strafe Down");
        c = new GridBagConstraints();
        c.weightx = 0.7;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 0;
        panelStrafeDown.add(lblStrafeDown, c);

        btnStrafeDown = new IcyButton("C");
        btnStrafeDown.setFlat(true);
        btnStrafeDown.setHorizontalAlignment(SwingConstants.CENTER);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0.3;
        c.gridx = 1;
        c.gridy = 0;
        panelStrafeDown.add(btnStrafeDown, c);

        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, "Info", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        mainPanel.add(panel);
        panel.setLayout(new BorderLayout(0, 0));

        infoTextPane = new JTextPane();
        panel.add(infoTextPane, BorderLayout.CENTER);
    }

    public static FlyCamProps getInstance()
    {
        if (singleton == null)
            singleton = new FlyCamProps();
        return singleton;
    }

    public boolean isXAxisInverted()
    {
        return cboxInvertX.isSelected();
    }

    public boolean isYAxisInverted()
    {
        return cboxInvertY.isSelected();
    }

    public double getSpeed()
    {
        return sliderSpeed.getValue();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();
        if (source == btnForward)
        {
            int code = new InputDialog().res;
            if (code != -1 && code != KeyEvent.VK_ESCAPE)
            {
                keyForward = code;
                btnForward.setText(KeyEvent.getKeyText(code));
            }
        }
        else if (source == btnBackward)
        {
            int code = new InputDialog().res;
            if (code != -1 && code != KeyEvent.VK_ESCAPE)
            {
                keyBackward = code;
                btnBackward.setText(KeyEvent.getKeyText(code));
            }
        }
        else if (source == btnStrafeLeft)
        {
            int code = new InputDialog().res;
            if (code != -1 && code != KeyEvent.VK_ESCAPE)
            {
                keyStrafeLeft = code;
                btnStrafeLeft.setText(KeyEvent.getKeyText(code));
            }
        }
        else if (source == btnStrafeRight)
        {
            int code = new InputDialog().res;
            if (code != -1 && code != KeyEvent.VK_ESCAPE)
            {
                keyStrafeRight = code;
                btnStrafeRight.setText(KeyEvent.getKeyText(code));
            }
        }
        else if (source == btnStrafeUp)
        {
            int code = new InputDialog().res;
            if (code != -1 && code != KeyEvent.VK_ESCAPE)
            {
                keyStrafeUp = code;
                btnStrafeUp.setText(KeyEvent.getKeyText(code));
            }
        }
        else if (source == btnStrafeDown)
        {
            int code = new InputDialog().res;
            if (code != -1 && code != KeyEvent.VK_ESCAPE)
            {
                keyStrafeDown = code;
                btnStrafeDown.setText(KeyEvent.getKeyText(code));
            }
        }
        savePrefs();
    }

    // private void handleNewShortcut(IcyButton button)
    // {
    // int code = new InputDialog().res;
    // if (code == -1 || code == KeyEvent.VK_ESCAPE)
    // return;
    //
    // keyForward = code;
    // String text = KeyEvent.getKeyText(code);
    //
    // btnForward.setText(text);
    // }

    private void savePrefs()
    {
        prefs.putBoolean(PREF_X_INVERTED, cboxInvertX.isSelected());
        prefs.putBoolean(PREF_Y_INVERTED, cboxInvertY.isSelected());
        prefs.putInt(PREF_SPEED, sliderSpeed.getValue());
        prefs.putInt(PREF_KEY_FORWARD, keyForward);
        prefs.putInt(PREF_KEY_BACKWARD, keyBackward);
        prefs.putInt(PREF_KEY_STRAFE_LEFT, keyStrafeLeft);
        prefs.putInt(PREF_KEY_STRAFE_RIGHT, keyStrafeRight);
        prefs.putInt(PREF_KEY_STRAFE_UP, keyStrafeUp);
        prefs.putInt(PREF_KEY_STRAFE_DOWN, keyStrafeDown);
    }

    private void loadPrefs()
    {
        cboxInvertX.setSelected(prefs.getBoolean(PREF_X_INVERTED, false));
        cboxInvertY.setSelected(prefs.getBoolean(PREF_Y_INVERTED, false));
        sliderSpeed.setValue(prefs.getInt(PREF_SPEED, 10));

        keyForward = prefs.getInt(PREF_KEY_FORWARD, KeyEvent.VK_UP);
        btnForward.setText(KeyEvent.getKeyText(keyForward));

        keyBackward = prefs.getInt(PREF_KEY_BACKWARD, KeyEvent.VK_DOWN);
        btnBackward.setText(KeyEvent.getKeyText(keyBackward));

        keyStrafeLeft = prefs.getInt(PREF_KEY_STRAFE_LEFT, KeyEvent.VK_LEFT);
        btnStrafeLeft.setText(KeyEvent.getKeyText(keyStrafeLeft));

        keyStrafeRight = prefs.getInt(PREF_KEY_STRAFE_RIGHT, KeyEvent.VK_RIGHT);
        btnStrafeRight.setText(KeyEvent.getKeyText(keyStrafeRight));

        keyStrafeUp = prefs.getInt(PREF_KEY_STRAFE_UP, KeyEvent.VK_PAGE_UP);
        btnStrafeUp.setText(KeyEvent.getKeyText(keyStrafeUp));

        keyStrafeDown = prefs.getInt(PREF_KEY_STRAFE_DOWN, KeyEvent.VK_PAGE_DOWN);
        btnStrafeDown.setText(KeyEvent.getKeyText(keyStrafeDown));
    }

    @Override
    public void onClosed()
    {
        savePrefs();
        singleton = null;

        // remove all overlays from sequence
        for (Sequence seq : Icy.getMainInterface().getSequences())
        {
            for (Overlay overlay : seq.getOverlays(FlyCamOverlay.class))
            {
                // stop thread
                ((FlyCamOverlay) overlay).stopThreads();
                // and remove overlay
                seq.removeOverlay(overlay);
            }
        }
    }

    private class InputDialog extends JDialog
    {
        int res = -1;

        public InputDialog()
        {
            JPanel panel = new JPanel();
            panel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
            panel.setLayout(new BorderLayout());
            JLabel label = new JLabel("Please hit the key you want to use.");
            panel.add(label);

            setContentPane(panel);
            addKeyListener(new KeyAdapter()
            {
                @Override
                public void keyPressed(KeyEvent e)
                {
                    res = e.getKeyCode();
                    setVisible(false);
                }
            });
            pack();
            setLocationRelativeTo(Icy.getMainInterface().getDesktopPane());
            setModal(true);
            setVisible(true);
        }
    }
}
