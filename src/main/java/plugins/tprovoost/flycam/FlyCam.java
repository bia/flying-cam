package plugins.tprovoost.flycam;

import icy.gui.dialog.MessageDialog;
import icy.plugin.abstract_.PluginActionable;
import icy.sequence.Sequence;

public class FlyCam extends PluginActionable
{
    @Override
    public void run()
    {
        Sequence s = getActiveSequence();

        if (s == null)
        {
            MessageDialog.showDialog("You must have a sequence opened for this operation.");
            return;
        }

        if (s.getOverlays(FlyCamOverlay.class).isEmpty())
            s.addOverlay(new FlyCamOverlay());

        final FlyCamProps props = FlyCamProps.getInstance();
        if (!props.isVisible())
        {
            props.addToDesktopPane();
            props.setVisible(true);
            props.toFront();
        }
    }
}
