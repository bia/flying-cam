package plugins.tprovoost.flycam;

import icy.canvas.IcyCanvas;
import icy.gui.main.GlobalOverlayListener;
import icy.gui.main.MainInterface;
import icy.main.Icy;
import icy.painter.Overlay;
import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;
import icy.type.point.Point5D;
import icy.vtk.IcyVtkPanel;

import java.awt.AWTException;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import plugins.kernel.canvas.VtkCanvas;
import vtk.vtkCamera;

public class FlyCamOverlay extends Overlay
{
    enum MovementType
    {
        MOVE_FORWARD, MOVE_BACKWARD, MOVE_STRAFE_LEFT, MOVE_STRAFE_RIGHT, MOVE_STRAFE_UP, MOVE_STRAFE_DOWN
    };

    // private double desiredUpdateRate = 25;
    HashMap<Integer, Boolean> keys = new HashMap<Integer, Boolean>();

    boolean tracking_mouse = false;
    double user_speed;

    // mouse previous coordinates
    private int lastMouseX = -1;
    private int lastMouseY = -1;

    /** Thread used for movement */
    private final MovementThread movementThread;

    /** Used to know if a overlay has been removed */
    GlobalOverlayListener overlayListener;

    /** Used to know when the window lost focus, to avoid remanent keys. */
    WindowFocusListener windowFocusListener;

    public FlyCamOverlay()
    {
        super("FlyCam");

        overlayListener = new GlobalOverlayListener()
        {
            @Override
            public void overlayAdded(Overlay overlay)
            {
                // nothing here
            }

            @Override
            public void overlayRemoved(Overlay overlay)
            {
                // we just removed this overlay ?
                if (overlay == FlyCamOverlay.this)
                {
                    stopThreads();

                    final MainInterface gui = Icy.getMainInterface();

                    gui.removeGlobalOverlayListener(overlayListener);
                    gui.getMainFrame().removeWindowFocusListener(windowFocusListener);
                }
            }
        };
        windowFocusListener = new WindowFocusListener()
        {
            @Override
            public void windowLostFocus(WindowEvent e)
            {
                keys.clear();
            }

            @Override
            public void windowGainedFocus(WindowEvent e)
            {
                //
            }
        };

        final MainInterface gui = Icy.getMainInterface();

        gui.addGlobalOverlayListener(overlayListener);
        gui.getMainFrame().addWindowFocusListener(windowFocusListener);

        movementThread = new MovementThread(null);
        ThreadUtil.bgRun(movementThread);
    }

    @Override
    public void mouseMove(MouseEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    {
        if (e.isConsumed())
            return;

        if (canvas instanceof VtkCanvas && tracking_mouse)
        {
            final IcyVtkPanel panel = ((VtkCanvas) canvas).getVtkPanel();

            // get current mouse position
            final int x = e.getX();
            final int y = e.getY();

            if ((lastMouseX != -1) && (lastMouseY != -1))
            {
                // rotate world from mouse movement
                rotateWorld((VtkCanvas) canvas, FlyCamProps.getInstance().isXAxisInverted() ? (lastMouseX - x)
                        : (x - lastMouseX), FlyCamProps.getInstance().isYAxisInverted() ? (lastMouseY - y)
                        : (y - lastMouseY), true);
            }

            // Test if should put back the cursor in the center.
            // This allows a smoother movement when approaching boarders.
            Point eventLocation = e.getLocationOnScreen();
            Point locationPanel = panel.getLocationOnScreen();
            boolean aboutToTouchX = eventLocation.x <= locationPanel.x + 1
                    || eventLocation.x >= locationPanel.x + panel.getWidth() - 1;
            boolean aboutToTouchY = eventLocation.y <= locationPanel.y + 1
                    || eventLocation.y >= locationPanel.y + panel.getHeight() - 1;

            if (aboutToTouchX || aboutToTouchY)
            {
                // nearly touching borders
                try
                {
                    Robot robot = new Robot();
                    lastMouseX = -1;
                    lastMouseY = -1;
                    robot.mouseMove(locationPanel.x + (panel.getWidth() / 2), locationPanel.y + (panel.getHeight() / 2));
                }
                catch (AWTException e1)
                {
                    //
                }
            }
            else
            {
                // normal case scenario
                lastMouseX = x;
                lastMouseY = y;
            }
        }
    }

    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        if (canvas instanceof VtkCanvas)
        {
            // update canvas if needed
            if (movementThread.canvas != canvas)
                movementThread.canvas = (VtkCanvas) canvas;

            user_speed = FlyCamProps.getInstance().getSpeed() * sequence.getWidth() * sequence.getPixelSizeX()
                    * 0.0001d;
        }

        super.paint(g, sequence, canvas);
    }

    @Override
    public void mouseClick(MouseEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    {
        if (e.isConsumed())
            return;

        if ((canvas instanceof VtkCanvas) && (e.getButton() == MouseEvent.BUTTON1))
        {
            tracking_mouse = !tracking_mouse;

            movementThread.canvas = (VtkCanvas) canvas;

            if (tracking_mouse)
            {
                // Transparent 16 x 16 pixel cursor image.
                final BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                // Create a new blank cursor.
                final Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0),
                        "blank cursor");
                ((VtkCanvas) canvas).getViewComponent().setCursor(blankCursor);
            }
            else
            {
                lastMouseX = -1;
                lastMouseY = -1;
                ((VtkCanvas) canvas).getViewComponent().setCursor(null);
                keys.clear();
            }

            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    {
        if (e.isConsumed())
            return;

        if (canvas instanceof VtkCanvas)
        {
            keys.put(e.getKeyCode(), false);
            e.consume();
        }
    }

    // @Override
    // public void mouseDrag(MouseEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    // {
    // if (e.isConsumed())
    // return;
    //
    // // just consume event to take priority
    // if ((canvas instanceof VtkCanvas) && tracking_mouse)
    // e.consume();
    // }

    @Override
    public void mouseEntered(MouseEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    {
        lastMouseX = -1;
        lastMouseY = -1;
    }

    @Override
    public void mouseExited(MouseEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    {
        if (canvas instanceof VtkCanvas)
        {
            if (tracking_mouse)
            {
                final IcyVtkPanel panel = ((VtkCanvas) canvas).getVtkPanel();
                final Point locationPanel = panel.getLocationOnScreen();

                try
                {
                    Robot robot = new Robot();
                    robot.mouseMove(locationPanel.x + panel.getWidth() / 2, locationPanel.y + panel.getHeight() / 2);
                }
                catch (AWTException e1)
                {
                    //
                }

                lastMouseX = -1;
                lastMouseY = -1;
            }
            else
                keys.clear();
        }
    }

    // @Override
    // public void mouseWheelMoved(MouseWheelEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    // {
    // //
    // }

    // void updateCamera(final VtkCanvas canvas)
    // {
    // ThreadUtil.invokeLater(new Runnable()
    // {
    // @Override
    // public void run()
    // {
    // generateVTKCamera(canvas.getVtkPanel());
    // }
    // });
    // }

    // void initCamera(VtkCanvas canvas)
    // {
    // target = new Vector3d();
    // position = new Vector3d();
    // forward = new Vector3d();
    // left = new Vector3d();
    // up = new Vector3d();
    //
    // if (canvas != null)
    // {
    // final IcyVtkPanel vtkPanel = canvas.getVtkPanel();
    //
    // position.x = canvas.getSequence().getWidth() / 2 * canvas.getSequence().getPixelSizeX();
    // position.y = canvas.getSequence().getHeight() / 2 * canvas.getSequence().getPixelSizeY();
    // position.z = canvas.getSequence().getSizeZ() * 10 * canvas.getSequence().getPixelSizeZ();
    //
    // // position.x = canvas.getSequence().getWidth() * 2 *
    // // canvas.getSequence().getPixelSizeX();
    // // position.y = canvas.getSequence().getHeight() * 2 *
    // // canvas.getSequence().getPixelSizeY();
    // // position.z = canvas.getSequence().getSizeZ() * 2 *
    // // canvas.getSequence().getPixelSizeZ();
    //
    // // init du vecteur vertical
    // up.x = 0.0d;
    // up.y = 0.0d;
    // up.z = 1.0d;
    //
    // // int Angle de vue
    //
    // target.x = canvas.getSequence().getWidth() / 2 * canvas.getSequence().getPixelSizeX();
    // target.y = canvas.getSequence().getHeight() / 2 * canvas.getSequence().getPixelSizeY();
    // target.z = canvas.getSequence().getSizeZ() / 2 * canvas.getSequence().getPixelSizeZ();
    //
    // forward.x = (target.x - position.x) / 100d;
    // forward.y = (target.y - position.y) / 100d;
    // forward.z = (target.z - position.z) / 100d;
    // forward.normalize();
    //
    // view_rotx = Math.toDegrees(Math.asin(forward.z));
    // double rtmp = Math.cos(Math.asin(forward.z));
    // view_roty = -Math.toDegrees(Math.acos(forward.x / rtmp));
    //
    // rtmp = Math.cos(Math.toRadians(view_rotx));
    // forward.z = Math.sin(Math.toRadians(view_rotx));
    // forward.x = rtmp * Math.cos(Math.toRadians(view_roty));
    // forward.y = rtmp * Math.sin(Math.toRadians(view_roty));
    //
    // target.x = position.x + forward.x * 100;
    // target.y = position.y + forward.y * 100;
    // target.z = position.z + forward.z * 100;
    //
    // // view_rotx = Math.toDegrees(Math.asin(forward.z));
    // // double rtmp = Math.cos(Math.asin(forward.z));
    // // view_roty = -Math.toDegrees(Math.acos(forward.x / rtmp));
    //
    // camera = vtkPanel.getCamera();
    //
    // vtkPanel.lock();
    // try
    // {
    // camera.SetViewUp(0, -1, 0);
    // camera.SetPosition(canvas.getSequence().getSizeX() / 2 * canvas.getSequence().getPixelSizeX(),
    // canvas.getSequence().getSizeY() / 2 * canvas.getSequence().getPixelSizeY(),
    // canvas.getSequence().getSizeX() * canvas.getSequence().getPixelSizeX() * 4);
    // camera.SetFocalPoint(canvas.getSequence().getSizeX() / 2 * canvas.getSequence().getPixelSizeX(),
    // canvas.getSequence().getSizeY() / 2 * canvas.getSequence().getPixelSizeY(),
    // canvas.getSequence().getSizeZ() / 2 * canvas.getSequence().getPixelSizeZ());
    // vtkPanel.resetCameraClippingRange();
    // }
    // finally
    // {
    // vtkPanel.unlock();
    // }
    //
    // // need repaint
    // vtkPanel.repaint();
    // }
    // }
    //
    // public void generateVTKCamera(IcyVtkPanel vtkPanel)
    // {
    // // vtkOpenGLCamera c = new vtkOpenGLCamera();
    // computeForwardVector();
    //
    // target.x = position.x + forward.x * 100;
    // target.y = position.y + forward.y * 100;
    // target.z = position.z + forward.z * 100;
    //
    // // System.out.println("------ generate vtk camera");
    // //
    // // System.out.println("view rot x : " + view_rotx);
    // // System.out.println("view rot y : " + view_roty);
    // //
    // // System.out.println("position : " + position.x);
    // // System.out.println("position : " + position.y);
    // // System.out.println("position : " + position.z);
    // //
    // // System.out.println("forwardx : " + forward.x);
    // // System.out.println("forwardy : " + forward.y);
    // // System.out.println("forwardz : " + forward.z);
    // //
    // // System.out.println("target : " + target.x);
    // // System.out.println("target : " + target.y);
    // // System.out.println("target : " + target.z);
    // //
    // // System.out.println("up : " + up.x);
    // // System.out.println("up : " + up.y);
    // // System.out.println("up : " + up.z);
    //
    // final vtkCamera camera = vtkPanel.getCamera();
    //
    // // vtkPanel.lock();
    // // try
    // // {
    // // camera.SetPosition(position.x, position.y, position.z);
    // // camera.SetViewUp(up.x, up.y, up.z);
    // // camera.SetFocalPoint(target.x, target.y, target.z);
    // // vtkPanel.resetCameraClippingRange();
    // // }
    // // finally
    // // {
    // // vtkPanel.unlock();
    // // }
    //
    // // need repaint
    // vtkPanel.repaint();
    // }
    //
    // private void computeForwardVector()
    // {
    // double rtmp = Math.cos(Math.toRadians(view_rotx));
    // forward.z = Math.sin(Math.toRadians(view_rotx));
    // forward.x = rtmp * Math.cos(Math.toRadians(view_roty));
    // forward.y = rtmp * Math.sin(Math.toRadians(view_roty));
    // }

    // @SuppressWarnings("unused")
    // private void convertCameraToCamLocData(vtkCamera camera)
    // {
    // double[] camPos = camera.GetPosition();
    // double[] camUp = camera.GetViewUp();
    // double[] camTarget = camera.GetFocalPoint();
    //
    // position.x = camPos[0];
    // position.y = camPos[1];
    // position.z = camPos[2];
    //
    // up.x = 0.0d;
    // up.y = 0.0d;
    // up.z = 1.0d;
    //
    // target.x = camTarget[0];
    // target.y = camTarget[1];
    // target.z = camTarget[2];
    //
    // System.out.println("------ convertCameraToCamLocData");
    // System.out.println("position : " + position.x);
    // System.out.println("position : " + position.y);
    // System.out.println("position : " + position.z);
    //
    // System.out.println("target : " + target.x);
    // System.out.println("target : " + target.y);
    // System.out.println("target : " + target.z);
    //
    // System.out.println("up : " + up.x);
    // System.out.println("up : " + up.y);
    // System.out.println("up : " + up.z);
    //
    // forward.x = (target.x - position.x) / 100d;
    // forward.y = (target.y - position.y) / 100d;
    // forward.z = (target.z - position.z) / 100d;
    //
    // System.out.println("forwardx : " + forward.x);
    // System.out.println("forwardy : " + forward.y);
    // System.out.println("forwardz : " + forward.z);
    //
    // view_rotx = Math.toDegrees(Math.asin(forward.x));
    // double rtmp = Math.cos(Math.toRadians(view_rotx));
    // view_roty = Math.toDegrees(Math.asin(forward.y / rtmp));
    //
    // System.out.println("view rot x : " + view_rotx);
    // System.out.println("view rot y : " + view_roty);
    //
    // // camera.SetClippingRange(1, 4000);
    // }

    @Override
    public void keyPressed(KeyEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
    {
        if (e.isConsumed())
            return;

        if (!(canvas instanceof VtkCanvas) || !tracking_mouse)
            return;

        final int code = e.getKeyCode();
        final IcyVtkPanel panel = ((VtkCanvas) canvas).getVtkPanel();
        final FlyCamProps props = FlyCamProps.getInstance();

        keys.put(code, true);

        if (isDown(props.keyForward))
            e.consume();
        if (isDown(props.keyBackward))
            e.consume();
        if (isDown(props.keyStrafeLeft))
            e.consume();
        if (isDown(props.keyStrafeRight))
            e.consume();
        if (isDown(props.keyStrafeUp))
            e.consume();
        if (isDown(props.keyStrafeDown))
            e.consume();
        // if (isDown(KeyEvent.VK_SPACE))
        // e.consume();
    }

    boolean isDown(int keycode)
    {
        // System.out.println("isDown ? " + KeyEvent.getKeyText(keycode));
        Boolean res = keys.get(keycode);
        return res != null && res.booleanValue();
    }

    public void move(VtkCanvas canvas, MovementType moveType)
    {
        final IcyVtkPanel vtkPanel = canvas.getVtkPanel();

        switch (moveType)
        {
            case MOVE_FORWARD:
                vtkPanel.zoomView(1 + (user_speed / 200d));
                break;

            case MOVE_BACKWARD:
                vtkPanel.zoomView(1 - (user_speed / 200d));
                break;

            case MOVE_STRAFE_LEFT:
                vtkPanel.translateView(user_speed * 5d, 0);
                break;

            case MOVE_STRAFE_RIGHT:
                vtkPanel.translateView(-user_speed * 5d, 0);
                break;

            case MOVE_STRAFE_DOWN:
                vtkPanel.translateView(0, user_speed * 5d);
                break;

            case MOVE_STRAFE_UP:
                vtkPanel.translateView(0, -user_speed * 5d);
                break;
        }

        vtkPanel.repaint();
    }

    public void stopThreads()
    {
        movementThread.run = false;
    }

    public VtkCanvas getCanvas()
    {
        return movementThread.canvas;
    }

    /**
     * Return current camera position
     */
    public CameraPosition getCameraPosition()
    {
        final CameraPosition result = new CameraPosition();
        final VtkCanvas vtkCanvas = getCanvas();

        if (vtkCanvas != null)
        {
            final vtkCamera camera = vtkCanvas.getCamera();

            result.position = camera.GetPosition();
            result.focal = camera.GetFocalPoint();
            result.viewUp = camera.GetViewUp();
            result.viewAngle = camera.GetViewAngle();
        }

        return result;
    }

    /**
     * Set current camera position
     */
    public void setCameraPosition(CameraPosition cameraPos, boolean repaint)
    {
        final VtkCanvas vtkCanvas = getCanvas();
        if (vtkCanvas != null)
        {
            final IcyVtkPanel vtkPanel = vtkCanvas.getVtkPanel();
            final vtkCamera camera = vtkCanvas.getCamera();

            vtkPanel.lock();
            try
            {
                // set camera position
                camera.SetPosition(cameraPos.position);
                camera.SetFocalPoint(cameraPos.focal);
                camera.SetViewUp(cameraPos.viewUp);
                camera.SetViewAngle(cameraPos.viewAngle);

                camera.OrthogonalizeViewUp();
                vtkPanel.resetCameraClippingRange();
                // update axis camera
                vtkPanel.updateAxisView();
            }
            finally
            {
                vtkPanel.unlock();
            }

            if (repaint)
                vtkPanel.repaint();
        }
    }

    /**
     * Rotate world
     */
    public static void rotateWorld(VtkCanvas canvas, int dx, int dy, boolean repaint)
    {
        final IcyVtkPanel vtkPanel = canvas.getVtkPanel();
        final vtkCamera camera = vtkPanel.getCamera();

        vtkPanel.lock();
        try
        {
            // rotate camera
            camera.Yaw(dx / 10d);
            camera.Pitch(dy / 10d);
            // camera.Roll(dy);
            camera.OrthogonalizeViewUp();
            vtkPanel.resetCameraClippingRange();
            // update axis camera
            vtkPanel.updateAxisView();
        }
        finally
        {
            vtkPanel.unlock();
        }

        if (repaint)
            vtkPanel.repaint();
    }

    private class MovementThread implements Runnable
    {
        VtkCanvas canvas;
        boolean run;

        public MovementThread(VtkCanvas canvas)
        {
            this.canvas = canvas;
            run = true;
        }

        @Override
        public void run()
        {
            while (run)
            {
                ThreadUtil.sleep(10L);

                if (canvas != null)
                {
                    final FlyCamProps props = FlyCamProps.getInstance();

                    if (isDown(props.keyForward))
                        move(canvas, MovementType.MOVE_FORWARD);
                    else if (isDown(props.keyBackward))
                        move(canvas, MovementType.MOVE_BACKWARD);

                    if (isDown(props.keyStrafeLeft))
                        move(canvas, MovementType.MOVE_STRAFE_LEFT);
                    else if (isDown(props.keyStrafeRight))
                        move(canvas, MovementType.MOVE_STRAFE_RIGHT);

                    if (isDown(props.keyStrafeUp))
                        move(canvas, MovementType.MOVE_STRAFE_UP);
                    else if (isDown(props.keyStrafeDown))
                        move(canvas, MovementType.MOVE_STRAFE_DOWN);
                }
            }
        }
    }
}
