package plugins.tprovoost.flycam;

import icy.type.collection.array.ArrayUtil;
import icy.util.XMLUtil;

import org.w3c.dom.Element;

public class CameraPosition
{
    public static final String ID_POSX = "posx";
    public static final String ID_POSY = "posy";
    public static final String ID_POSZ = "posz";
    public static final String ID_FOCALX = "focalx";
    public static final String ID_FOCALY = "focaly";
    public static final String ID_FOCALZ = "focalz";
    public static final String ID_VIEWUPX = "viewupx";
    public static final String ID_VIEWUPY = "viewupy";
    public static final String ID_VIEWUPZ = "viewupz";
    public static final String ID_VIEWANGLE = "viewangle";
    public static final String ID_TIME = "time";

    public double[] position;
    public double[] focal;
    public double[] viewUp;
    public double viewAngle;
    public int timeIndex;

    public CameraPosition()
    {
        super();

        position = new double[3];
        focal = new double[3];
        viewUp = new double[3];
        viewAngle = 0d;
        timeIndex = 0;
    }

    public void saveCamera(Element e)
    {
        if (e != null)
        {
            XMLUtil.setElementDoubleValue(e, ID_POSX, position[0]);
            XMLUtil.setElementDoubleValue(e, ID_POSY, position[1]);
            XMLUtil.setElementDoubleValue(e, ID_POSZ, position[2]);
            XMLUtil.setElementDoubleValue(e, ID_FOCALX, focal[0]);
            XMLUtil.setElementDoubleValue(e, ID_FOCALY, focal[1]);
            XMLUtil.setElementDoubleValue(e, ID_FOCALZ, focal[2]);
            XMLUtil.setElementDoubleValue(e, ID_VIEWUPX, viewUp[0]);
            XMLUtil.setElementDoubleValue(e, ID_VIEWUPY, viewUp[1]);
            XMLUtil.setElementDoubleValue(e, ID_VIEWUPZ, viewUp[2]);
            XMLUtil.setElementDoubleValue(e, ID_VIEWANGLE, viewAngle);
            XMLUtil.setElementIntValue(e, ID_TIME, timeIndex);
        }
    }

    public void loadCamera(Element e)
    {
        if (e != null)
        {
            position[0] = XMLUtil.getElementDoubleValue(e, ID_POSX, 0d);
            position[1] = XMLUtil.getElementDoubleValue(e, ID_POSY, 0d);
            position[2] = XMLUtil.getElementDoubleValue(e, ID_POSZ, 0d);
            focal[0] = XMLUtil.getElementDoubleValue(e, ID_FOCALX, 0d);
            focal[1] = XMLUtil.getElementDoubleValue(e, ID_FOCALY, 0d);
            focal[2] = XMLUtil.getElementDoubleValue(e, ID_FOCALZ, 0d);
            viewUp[0] = XMLUtil.getElementDoubleValue(e, ID_VIEWUPX, 0d);
            viewUp[1] = XMLUtil.getElementDoubleValue(e, ID_VIEWUPY, 0d);
            viewUp[2] = XMLUtil.getElementDoubleValue(e, ID_VIEWUPZ, 0d);
            viewAngle = XMLUtil.getElementDoubleValue(e, ID_VIEWANGLE, 0d);
            timeIndex = XMLUtil.getElementIntValue(e, ID_TIME, 0);
        }
    }

    @Override
    public String toString()
    {
        return "Position=[" + ArrayUtil.array1DToString(position, true, false, ",", 5) + "]  focal=["
                + ArrayUtil.array1DToString(focal, true, false, ",", 5) + "]  view up=["
                + ArrayUtil.array1DToString(viewUp, true, false, ",", 5) + "]  view angle=" + viewAngle;
    }
}
